class Usuario {
    String refresh;
    String access;
    String username;
    bool authenticated;

    Usuario({this.refresh, this.access, this.username, this.authenticated});

    Usuario.fromJson(Map<String, dynamic> json) {
        refresh = json['refresh'];
        access = json['access'];
        username = json['username'];
        authenticated = json['authenticated'];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['refresh'] = this.refresh;
        data['access'] = this.access;
        data['username'] = this.username;
        data['authenticated'] = this.authenticated;
        return data;
    }

    String toString() {
        return 'Usuario(token: $access)';
    }
}
