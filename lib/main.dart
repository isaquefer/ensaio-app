import 'package:flutter/material.dart';

import 'package:app_ensaio/ui/home_page.dart';
import 'package:app_ensaio/ui/login_screen.dart';
import 'package:app_ensaio/services/login_api.dart';

LoginApi appAuth = new LoginApi();

void main() async {

    WidgetsFlutterBinding.ensureInitialized();

    Widget _defaultHome = new LoginScreen();

    String _result = await appAuth.isLogged();
    if (_result != "") {
        _defaultHome = new HomePage();
    }

    runApp(new MaterialApp(
        title: 'App',
        home: _defaultHome,
    ));
}
