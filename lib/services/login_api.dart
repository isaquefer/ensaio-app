import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:app_ensaio/models/usuario.dart';
import 'package:app_ensaio/utils/shared_pref.dart';


class LoginApi {
    static Future<Usuario> login(String user, String password) async {
        var url = 'http://192.168.0.60:8080/api/token/';
        var header = {"Content-Type": "application/json"};
        var usuario;
        SharedPref sharedPref = SharedPref();

        Map params = {
            'username': user,
            'password': password
        };

        var _body = json.encode(params);
        var response = await http.post(url, headers:header, body: _body);

        Map mapResponse = json.decode(response.body);
        if (response.statusCode == 200) {
            usuario = Usuario.fromJson(mapResponse);
            usuario.username = user;
            usuario.authenticated = true;

            sharedPref.save("user", usuario);
        } else {
            usuario = null;
        }

        return usuario;
    }

    isLogged() async {
        final prefs = await SharedPreferences.getInstance();
        var logged = (prefs.getString("user") ?? "");
        return logged;
    }

    logout() async {
        final prefs = await SharedPreferences.getInstance();
        prefs.remove("user");
        return "";
    }
}
