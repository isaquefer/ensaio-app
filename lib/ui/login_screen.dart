import 'package:flutter/material.dart';

import 'package:app_ensaio/ui/home_page.dart';
import 'package:app_ensaio/services/login_api.dart';

class LoginScreen extends StatefulWidget {

    @override
    _LoginScreenState createState() => _LoginScreenState();
}


class _LoginScreenState extends State<LoginScreen> {

    final _ctrlLogin = TextEditingController();
    final _ctrlPassword = TextEditingController();
    final _formKey = GlobalKey<FormState>();

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            body: Container(
                padding: EdgeInsets.only(top: 60, left: 40, right: 40),
                color: Colors.white,
                child: Form(
                    key: _formKey,
                    child: ListView(
                        children: <Widget>[
                        SizedBox(
                            width: 128,
                            height: 128,
                            /* child: Image.asset("assets/logo.png"), */
                        ),
                        SizedBox(
                            height: 20,
                        ),
                        TextFormField(
                            // autofocus: true,
                            keyboardType: TextInputType.text,
                            controller: _ctrlLogin,
                            decoration: InputDecoration(
                                labelText: "Username",
                                labelStyle: TextStyle(
                                    color: Colors.black38,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 20,
                                ),
                            ),
                            style: TextStyle(fontSize: 20),
                        ),
                        SizedBox(
                            height: 10,
                        ),
                        TextFormField(
                            // autofocus: true,
                            keyboardType: TextInputType.text,
                            obscureText: true,
                            controller: _ctrlPassword,
                            decoration: InputDecoration(
                                labelText: "Senha",
                                labelStyle: TextStyle(
                                    color: Colors.black38,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 20,
                                ),
                            ),
                            style: TextStyle(fontSize: 20),
                        ),
                        Container(
                            height: 40,
                            alignment: Alignment.centerRight,
                            child: FlatButton(
                                child: Text(
                                    "Recuperar Senha",
                                    textAlign: TextAlign.right,
                                ),
                                onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            /* builder: (context) => ResetPasswordPage(), */
                                        ),
                                    );
                                },
                            ),
                        ),
                        SizedBox(
                            height: 40,
                        ),
                        Container(
                            height: 60,
                            alignment: Alignment.centerLeft,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                    stops: [0.3, 1],
                                    colors: [
                                        Color(0xFFF58524),
                                        Color(0XFFF92B7F),
                                    ],
                                ),
                                borderRadius: BorderRadius.all(
                                    Radius.circular(5),
                                ),
                            ),
                            child: SizedBox.expand(
                                child: FlatButton(
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                            Text(
                                                "Login",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                ),
                                                textAlign: TextAlign.left,
                                            ),
                                            Container(
                                                child: SizedBox(
                                                    /* child: Image.asset("assets/bone.png"), */
                                                    height: 28,
                                                    width: 28,
                                                ),
                                            )
                                        ],
                                    ),
                                    onPressed: () {
                                        _clickButton(context);
                                    },
                                ),
                            ),
                        ),
                        SizedBox(
                            height: 10,
                        ),
                        Container(
                            height: 60,
                            alignment: Alignment.centerLeft,
                            decoration: BoxDecoration(
                                color: Color(0xFF3C5A99),
                                borderRadius: BorderRadius.all(
                                    Radius.circular(5),
                                ),
                            ),
                            child: SizedBox.expand(
                                child: FlatButton(
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                            Text(
                                                "Login com Facebook",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white,
                                                    fontSize: 20,
                                                ),
                                                textAlign: TextAlign.left,
                                            ),
                                            Container(
                                                child: SizedBox(
                                                    /* child: Image.asset("assets/fb-icon.png"), */
                                                    height: 28,
                                                    width: 28,
                                                ),
                                            )
                                        ],
                                    ),
                                    onPressed: () {},
                                ),
                            ),
                        ),
                        SizedBox(
                            height: 10,
                        ),
                        Container(
                            height: 40,
                            child: FlatButton(
                                child: Text(
                                    "Cadastre-se",
                                    textAlign: TextAlign.center,
                                ),
                                onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            /* builder: (context) => SignupPage(), */
                                        ),
                                    );
                                },
                            ),
                        ),
                    ],
                ),
            ),
        ),
        );
    }

    void _clickButton(BuildContext context) async {
        bool formOk = _formKey.currentState.validate();

        if (!formOk) {
            return;
        }

        String login = _ctrlLogin.text;
        String password = _ctrlPassword.text;

        var usuario = await LoginApi.login(login, password);
        if (usuario != null) {
            _navegaHomePage(context);
        } else {
            // colocar um alert aqui
        }
    }

    _navegaHomePage(BuildContext context) {
        Navigator.push(
            context, MaterialPageRoute(
                builder: (context) => HomePage()
            ),
        );
    }
}
