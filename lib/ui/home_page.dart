import 'package:flutter/material.dart';

import 'package:app_ensaio/ui/contar_instrumentos.dart';
import 'package:app_ensaio/ui/login_screen.dart';

class HomePage extends StatefulWidget {

    @override
    _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            home: Scaffold(
                appBar: AppBar(title: Text('First app')),
                body: Center(
                    child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                            Padding(
                                padding: EdgeInsets.all(10.0),
                                child: Builder(
                                    builder: (context) => RaisedButton(
                                        color: Colors.red,
                                        child: Text('Contar instrumentos'),
                                        onPressed: () {
                                            Navigator.push(context,
                                                MaterialPageRoute(builder: (context) => ContarInstrumentos())
                                            );
                                        },
                                    ),
                                ),
                            ),
                            Padding(
                                padding: EdgeInsets.all(10.0),
                                child: FlatButton(
                                    onPressed: () {},
                                    child: Text('Ensaios'),
                                    color: Colors.green,
                                ),
                            ),
                        ]
                    ),
                ),
            )
        );
    }
}
